<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title> Restaurant </title>

    <link rel="stylesheet" href="../../Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="../../Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"> </script>
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->

    <style>

        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: lightgrey;
        }

        li {
            float: right;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 30px 30px;
            text-decoration: none;
        }

        li a:hover {
            background-color: darkslategrey;
            color : white;
        }

        h3{
            position: absolute;
            top: 300px;
            left: 0;
            width: 100%;
            color: darkslategrey;
            font-size: 300%;
            background: rgba(255, 255, 255, 0.75);
        }
         .slide{

            background-repeat: no-repeat;

        }

    </style>

</head>

<body>
<div>
    <nav class="navigation-bar">

        <ul>
            <li><a href='food/foodmenu.php'>USER</a></li>
            <li><a href='admin.php'>ADMIN</a></li>
            <i><h1 style=" color:purple; margin-left: 400px"> DELISH &nbsp; DIARIES <br> </h1></i>
        </ul>
    </nav>
</div>

<div class="container">
    <h3  style="text-align: center"> BEST FOOD IN CITY  </h3>
</div>

    <div class="slide">
        <img class="mySlides" src="../../Resources/images/8.jpg" style="width:100%; height: 700px">
        <img class="mySlides" src="../../Resources/images/1.jpg" style="width:100%; height: 700px">
        <img class="mySlides" src="../../Resources/images/2.jpeg" style="width:100%; height: 700px">
        <img class="mySlides" src="../../Resources/images/4.jpg" style="width:100%; height: 700px">
        <img class="mySlides" src="../../Resources/images/7.jpg" style="width:100%; height: 700px">
        <img class="mySlides" src="../../Resources/images/6.jpg" style="width:100%; height: 700px">
        <img class="mySlides" src="../../Resources/images/3.jpg" style="width:100%; height: 700px">
        <img class="mySlides" src="../../Resources/images/5.jpg" style="width:100%; height: 700px">
    </div>



    <script>
        var myIndex = 0;
        carousel();

        function carousel() {
            var i;
            var x = document.getElementsByClassName("mySlides");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            myIndex++;
            if (myIndex > x.length) {myIndex = 1}
            x[myIndex-1].style.display = "block";
            setTimeout(carousel, 1000); // Change image every 2 seconds
        }
    </script>

</body>
</html>