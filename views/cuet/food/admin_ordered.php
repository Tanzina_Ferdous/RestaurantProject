<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Message\Message;
use App\Restaurant\Food;


$obj = new Food();
$allData  =  $obj->trashed();


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>


    <link rel="stylesheet" href="../../../Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <script src="../../../Resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

    <style>

        body {

            background-image: url("../../../Resources/images/b3.jpg");
            background-repeat: no-repeat;
            background-size: cover;
        }

    </style>

</head>

<body>


<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center" >
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>

<div class="container">

<form id="multiple" method="post">

    <div style="margin-left: 65%" class="nav navbar">

        <a href='paid.php' class='btn btn-lg bg-danger'> Paid Orders </a>
        <a href='unpaid.php' class='btn btn-lg bg-danger'>Unpaid Orders</a>


    </div>



    <div class="bg-info text-center"></div>
    <h1> Ordered Items List</h1>

    <table border="1px" class="table table-bordered table-striped">

        <tr style="font-size: larger">

            <th> Serial </th>
            <th> Item NO </th>
            <th> Item Name </th>
            <th> Quantity </th>
            <th> Price </th>
        </tr>



        <?php


         $serial=1;

         foreach ($allData as $oneData){

             if($serial%2) $bgColor ="lightgoldenrodyellow";
             else $bgColor = "#ffffff";

             echo "
    
                        <tr  style='background-color: $bgColor; background: rgba(200,200,200,0.2); font-size: larger'>
    
                       
                        <td style='width: 5%; text-align: center'>$serial</td>
                        <td style='width: 10%; text-align: center'>$oneData->ID</td>
                        <td style='width: 20%;'>$oneData->NAME</td>
                        <td style='width: 15%;'>$oneData->QUANTITY</td>
                        
                        <td style='width: 10%;'>$oneData->PRICE</td>
                                     
    
                                     <td style='width: 15%'>
                                      
                                       <a href='pay.php?id=$oneData->ID' class='btn btn-success'> Paid </a>
                                       <a href='unpay.php?id=$oneData->ID' class='btn btn-success'> Unpaid </a>
                                       
                                     </td>
                                  </tr>
                              ";
             $serial++;

         }

       ?>

    </table>

</form>


</div>

<script src="../../Resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

<script>




    $(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);

    });


    });


</script>

</body>
</html>