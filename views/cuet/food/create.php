<?php
require_once ("../../../vendor/autoload.php");

if(!isset($_SESSION)) session_start();

use App\Message\Message;

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title> Food Entry </title>

    <link rel="stylesheet" href="../../../Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="../../../Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>


</head>

<body>

<div style="height: 50px">
    <div id="message" class="btn-success text-center">
        <?php

        echo Message::message();

        ?>
    </div>
</div>

<div class="container">

    <form id="multiple" method="post">

        <div class="nav navbar">

            <a href='food.php' class='btn btn-lg bg-danger'>Active Employee List</a>

        </div>
    </form>
</div>

<div>

    <h1 style="color: #f5e79e; margin-left: 300px; "> Employee - Add Form </h1>

    <div class="col-md-2"> </div>

    <div class="col-md-8" style="margin-left: -50px">

        <form action="store.php" method="post">
            <div class="form-group">
                <label style="color: #f5e79e" > Employee Name </label>
                <input style="width: 500px" type="text" class="form-control" name="EmployeeName" placeholder="Enter Employee Name Here">
            </div>
            <div class="form-group">
                <label style="color: #f5e79e" > Shift</label>
                <input style="width: 500px" type="text" class="form-control" name="Shift" placeholder="Enter Shift Here">
            </div>

            <div class="form-group">
                <label style="color: #f5e79e" > Salary</label>
                <input style="width: 500px" type="text" class="form-control" name="Salary" placeholder="Enter Salary Here">
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>

</div>

<div class="col-md-2"></div>

<script src="../../Resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>

</body>
</html>
