<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 11/4/2017
 * Time: 10:31 PM
 */

namespace App\Restaurant;
use App\Message\Message;
use App\Model\Database;
use App\Utility\Utility;
use PDO;



class Food extends Database
{
    public $F_id, $F_Type, $F_Price, $F_Quan, $F_Name, $F_Des;

    public function setData($postArray){

        if(array_key_exists("id",$postArray))
            $this->F_id = $postArray['id'];

        if(array_key_exists("EmployeeName",$postArray))
            $this->F_Name = $postArray['EmployeeName'];

        if(array_key_exists("Shift",$postArray))
            $this->E_Type = $postArray['Shift'];

        if(array_key_exists("Salary",$postArray))
            $this->E_Price = $postArray['Salary'];

    }

    public function store(){

        $sqlQuery = "INSERT INTO menu (Name, Shift, Salary) VALUES ( ?,?,? )";

        $dataArray = [ $this->E_Name, $this->E_shift, $this->E_salary ];

        $sth =  $this->dbh->prepare($sqlQuery);

        $status =  $sth->execute($dataArray);


        if($status)
            Message::setMessage("Success! Data has been inserted successfully. <br>");
        else
            Message::setMessage("Failed! Data has not been inserted. <br>");

    }

    public function index(){

        $sqlQuery = "SELECT * FROM menu WHERE is_trashed='NO'";

        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

    public function indexitalian(){

        $sqlQuery = "SELECT * FROM menu WHERE TYPE ='Italian'";

        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

    public function indexmexican(){

        $sqlQuery = "SELECT * FROM menu WHERE TYPE ='Mexican'";

        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

    public function indexdessert(){

        $sqlQuery = "SELECT * FROM menu WHERE TYPE ='Dessert'";

        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

    public function indexbeverage(){

        $sqlQuery = "SELECT * FROM menu WHERE  TYPE ='Beverages'";

        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

    // trash means order
    public function trash(){

        $sqlQuery = "UPDATE menu SET is_trashed=NOW() WHERE ID=".$this->F_id;

        $status = $this->dbh->exec($sqlQuery);

        if($status)
            Message::setMessage("Success! Item has been ordered successfully. <br>");
        else
            Message::setMessage("Failed! Item has not been ordered. <br>");

    }

    public function trashed(){

        $sqlQuery = "Select * from menu WHERE is_trashed <> 'NO'";


        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

    public function trashMultiple($selectedIDs){

        if( count($selectedIDs) == 0) {
            Message::message("Empty Selection! Please Select Some Item(s).");
            return;
        }

        $status = true;
        foreach ($selectedIDs as $id){

            $sqlQuery = "UPDATE menu SET is_trashed=NOW() WHERE ID=$id"  ;

            if ( ! $this->dbh-> exec($sqlQuery) )
                $status = false;
        }


        if($status)
            Message::message("Success! All Seleted Items Has Been Ordered");
        else
            Message::message("Failed! All Seleted Items Has Not Been Ordered");

    }

    //recover means unorder
    public function recover(){


        $sqlQuery = "UPDATE menu SET is_trashed='NO' WHERE ID=".$this->F_id;

        $status = $this->dbh->exec($sqlQuery);

        if($status)
            Message::setMessage("Success! Item has been unordered successfully. <br>");
        else
            Message::setMessage("Failed! Item has not been unordered. <br>");

    }

    public function recoverMultiple($selectedIDs){

        if( count($selectedIDs) == 0) {
            Message::message("Empty Selection! Please Select Some Record(s).");
            return;
        }

        $status = true;
        foreach ($selectedIDs as $id){

            $sqlQuery = "UPDATE menu SET is_trashed='NO' WHERE ID=$id"  ;

            if ( ! $this->dbh-> exec($sqlQuery) )
                $status = false;
        }


        if($status)
            Message::message("Success! All Seleted Items Has Been Unordered");
        else
            Message::message("Failed! All Seleted Items Has Not Been Unordered");

    }

    public function pay(){

        $sqlQuery = "UPDATE menu SET is_paid= 'YES' WHERE ID=".$this->F_id;

        $status = $this->dbh->exec($sqlQuery);

        if($status)
            Message::setMessage("Success! Item has been paid successfully. <br>");
        else
            Message::setMessage("Failed! Item has not been paid. <br>");

    }

    public function paid (){

        $sqlQuery = "Select * from menu WHERE is_paid='YES'";


        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

    public function unpay(){

        $sqlQuery = "UPDATE menu SET is_paid='Unpaid' WHERE ID=".$this->F_id;

        $status = $this->dbh->exec($sqlQuery);

        if($status)
            Message::setMessage("Success! Item has not been paid . <br>");
        else
            Message::setMessage("Failed! Item has not been paid. <br>");

    }

    public function unpaid (){

        $sqlQuery = "Select * from menu WHERE is_paid= 'Unpaid'";


        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

}