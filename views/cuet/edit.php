<?php
   require_once ("../../vendor/autoload.php");
   if(!isset($_SESSION)) session_start();
   use App\Message\Message;
   use App\Restaurant\Restaurant;

   $obj = new Restaurant();
   $obj->setData($_GET);
   $oneData = $obj->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    
    <link rel="stylesheet" href="../../Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="../../Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script src="../../Resources/jquery-ui-1.12.1.custom"></script>

    <style>

        body {

            background-image: url("../../Resources/images/b9.jpg");
            background-repeat: no-repeat;
            background-size: cover;
        }

    </style>
    
</head>
<body>

<div id="message" class="bg-primary text-center" > <?php echo Message::message() ?> </div>

<div style="margin-top: 100px">

    <h1 style="text-align: center; color: #f7ecb5"> Employee - Edit Form </h1>

    <div class="col-md-2"> </div>


    <div class="col-md-6" style="margin-top: 50px; margin-bottom: 50px; margin-left: 150px">


        <form action="update.php" method="post">
         

            
            <div class="form-group">
                <label for="Employee Name" style=" color: #f7ecb5"> <h3>Employee Name</h3></label>
                <input type="text" class="form-control" name="EmployeeName" value="<?php echo $oneData->Name ?>">
            </div>


            <div class="form-group">
                <label for="Shift" style=" color: #f7ecb5"> <h3> Shift </h3></label>
                <input type="text" class="form-control" name="Shift" value="<?php echo $oneData->Shift ?>">
            </div>

            <div class="form-group">
                <label for="Salary" style=" color: #f7ecb5"><h3>Salary</h3></label>
                <input type="text" class="form-control" name="Salary" value="<?php echo $oneData->Salary ?>">
            </div>


            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $oneData->ID ?>"
            </div>


            <button type="submit" class="btn btn-default">Update</button>




        </form>

    </div>


    <div class="col-md-2" > </div>

</div>


<script>


    $(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    });


</script>

</body>
</html>