<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Message\Message;
use App\Restaurant\Food;
use App\Utility\Utility;


$obj = new Food();
$allData  =  $obj->index();
$allDataItalian  =  $obj->indexitalian();
$allDataMexican  =  $obj->indexmexican();
$allDataDessert  =  $obj->indexdessert();
$allDataBeverage  =  $obj->indexbeverage();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>document</title>

    <link rel="stylesheet" href="../style.css">

    <link rel="stylesheet" href="../../../Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <script src="../../../Resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

    <script src="../../../Resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <style>

        body {

            background-image: url("../../../Resources/images/9.jpg");
            background-repeat: no-repeat;
            background-size: cover;
        }

    </style>

</head>

<body>


    <div style="color:goldenrod ; font-size: 200%; margin-left: 500px "><i><h1> FOOD MENU</h1></i></div>

    <div class="container">

        <form id="multiple"  method="post">


            <div class="nav navbar" style="margin-left: 60%">

                <button id="TrashSelected" class='btn btn-lg bg-danger' style="color: darkblue">Order Selected Items</button>
                <a href='ordered.php' class='btn btn-lg bg-danger'> Ordered Items </a>

            </div>


    </div>

    <div style="color: green; margin-left: 100px "><h2> ITALIAN </h2></div>

    <table border="1px" class="table table-bordered table-striped">

        <tr style="font-size: larger">

            <th> Select </th>
            <th> Serial </th>
            <th> Item NO </th>
            <th> Item Name </th>
            <th> Quantity </th>
            <th> Description </th>
            <th> Price </th>

        </tr>

        <?php

        $serial=1;

        foreach ($allDataItalian as $oneData){

            if($serial%2) $bgColor = "lightgoldenrodyellow";
            else $bgColor = "#ffffff";

            echo "
                        <tr  style='background-color: $bgColor ; background: rgba(200,200,200,0.6); font-size: larger'>
                        
                        <td style='width: 5%; padding-left: 2%'> <input type='checkbox' class='checkbox' name='mark[]' value='$oneData->ID'></td>
                        <td style='width: 5%; text-align: center'>$serial</td>
                        <td style='width: 10%; text-align: center'>$oneData->ID</td>
                        <td style='width: 20%;'>$oneData->NAME</td>
                        <td style='width: 10%;'>$oneData->QUANTITY</td>
                        <td style='width: 20%;'>$oneData->DESCRIPTION</td>
                        <td style='width: 10%;'>$oneData->PRICE</td>
    
                     </tr>
                                  
                   ";
            $serial++;
        }
        ?>

    </table>

    <div style="color: green; margin-left: 100px "><h2> MEXICAN </h2></div>

    <table border="1px" class="table table-bordered table-striped">

        <tr style="font-size: larger">

            <th> Select </th>
            <th> Serial </th>
            <th> Item NO </th>
            <th> Item Name </th>
            <th> Quantity </th>
            <th> Description </th>
            <th> Price </th>

        </tr>

        <?php

        $serial=1;

        foreach ($allDataMexican as $oneData){

            if($serial%2) $bgColor = "lightgoldenrodyellow";
            else $bgColor = "#ffffff";

            echo "
                        <tr  style='background-color: $bgColor ; background: rgba(200,200,200,0.6); font-size: larger'>
                        
                        <td style='width: 5%; padding-left: 2%'> <input type='checkbox' class='checkbox' name='mark[]' value='$oneData->ID'></td>                        
                        <td style='width: 5%; text-align: center'>$serial</td>
                        <td style='width: 10%; text-align: center'>$oneData->ID</td>
                        <td style='width: 20%;'>$oneData->NAME</td>
                        <td style='width: 10%;'>$oneData->QUANTITY</td>
                        <td style='width: 20%;'>$oneData->DESCRIPTION</td>
                        <td style='width: 10%;'>$oneData->PRICE</td>
    
                     </tr>
                                  
                   ";
            $serial++;
        }
        ?>

    </table>

    <div style="color: green; margin-left: 100px "><h2> DESSERT </h2></div>

    <table border="1px" class="table table-bordered table-striped">

        <tr style="font-size: larger">

            <th> Select </th>
            <th> Serial </th>
            <th> Item NO </th>
            <th> Item Name </th>
            <th> Quantity </th>
            <th> Description </th>

            <th> Price </th>

        </tr>

        <?php

        $serial=1;

        foreach ($allDataDessert as $oneData){

            if($serial%2) $bgColor = "lightgoldenrodyellow";
            else $bgColor = "#ffffff";

            echo "
                        <tr  style='background-color: $bgColor ; background: rgba(200,200,200,0.6); font-size: larger'>
                        
                        <td style='width: 5%; padding-left: 2%'> <input type='checkbox' class='checkbox' name='mark[]' value='$oneData->ID'></td>
                        <td style='width: 5%; text-align: center'>$serial</td>
                        <td style='width: 10%; text-align: center'>$oneData->ID</td>
                        <td style='width: 20%;'>$oneData->NAME</td>
                        <td style='width: 10%;'>$oneData->QUANTITY</td>
                        <td style='width: 20%;'>$oneData->DESCRIPTION</td>
                        
                        <td style='width: 10%;'>$oneData->PRICE</td>
    
                     </tr>
                                  
                   ";
            $serial++;
        }
        ?>

    </table>

    <div style="color: green; margin-left: 100px "><h2> BEVERAGES </h2></div>

    <table border="1px" class="table table-bordered table-striped">

        <tr style="font-size: larger">

            <th> Select </th>
            <th> Serial </th>
            <th> Item NO </th>
            <th> Item Name </th>
            <th> Quantity </th>
            <th> Description </th>

            <th> Price </th>

        </tr>

        <?php

        $serial=1;

        foreach ($allDataBeverage as $oneData){

            if($serial%2) $bgColor = "lightgoldenrodyellow";
            else $bgColor = "#ffffff";

            echo "
                        <tr  style='background-color: $bgColor ; background: rgba(200,200,200,0.6); font-size: larger'>
                        
                        <td style='width: 5%; padding-left: 2%'> <input type='checkbox' class='checkbox' name='mark[]' value='$oneData->ID'></td>
                        <td style='width: 5%; text-align: center'>$serial</td>
                        <td style='width: 10%; text-align: center'>$oneData->ID</td>
                        <td style='width: 20%;'>$oneData->NAME</td>
                        <td style='width: 10%;'>$oneData->QUANTITY</td>
                        <td style='width: 20%;'>$oneData->DESCRIPTION</td>
                        
                        <td style='width: 10%;'>$oneData->PRICE</td>
    
                     </tr>
                                  
                   ";
            $serial++;
        }
        ?>

    </table>
    </form>

    <script>

        $(document).ready(function () {


            $("#TrashSelected").click(function () {
                $("#multiple").attr('action','order_selected.php');
                $("#multiple").submit();
            });


            $("#RecoverSelected").click(function () {
                $("#multiple").attr('action', 'recover_selected.php');
                $("#multiple").submit();
            });


        });


        $(function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);

        });

        //select all checkboxes
        $("#select_all").change(function(){  //"select all" change
            var status = this.checked; // "select all" checked status
            $('.checkbox').each(function(){ //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });

        $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
            if(this.checked == false){ //if this item is unchecked
                $("#select_all")[0].checked = false; //change "select all" checked status to false
            }

//check "select all" if all checkbox items are checked
            if ($('.checkbox:checked').length == $('.checkbox').length ){
                $("#select_all")[0].checked = true; //change "select all" checked status to true
            }
        });

    </script>


</body>
</html>