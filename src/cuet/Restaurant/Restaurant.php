<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 10/30/2017
 * Time: 12:37 PM
 */

namespace App\Restaurant;
use App\Message\Message;
use App\Model\Database;
use App\Utility\Utility;
use PDO;


class Restaurant extends Database
{
    public $E_id, $E_salary, $E_Name, $E_shift, $a_name, $a_pass ;

    /*public function login($postArray){

        if(array_key_exists("txtname",$postArray))
            $this->a_name = $postArray['txtname'];

        if(array_key_exists("txtpass",$postArray))
            $this->a_pass = $postArray['txtpass'];

        $sqlQuery = "SELECT * FROM admin WHERE Name= ".$this->a_name;

        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $oneData =  $sth->fetch();

        return $oneData;

    }*/

    public function setData($postArray){

        if(array_key_exists("id",$postArray))
            $this->E_id = $postArray['id'];

        if(array_key_exists("EmployeeName",$postArray))
            $this->E_Name = $postArray['EmployeeName'];

        if(array_key_exists("Shift",$postArray))
            $this->E_shift = $postArray['Shift'];

        if(array_key_exists("Salary",$postArray))
            $this->E_salary = $postArray['Salary'];

    }

    public function store(){

        $sqlQuery = "INSERT INTO employee (Name, Shift, Salary) VALUES ( ?,?,? )";

        $dataArray = [ $this->E_Name, $this->E_shift, $this->E_salary ];

        $sth =  $this->dbh->prepare($sqlQuery);

        $status =  $sth->execute($dataArray);


        if($status)
            Message::setMessage("Success! Data has been inserted successfully. <br>");
        else
            Message::setMessage("Failed! Data has not been inserted. <br>");

    }

    public function index(){

        $sqlQuery = "SELECT * FROM employee WHERE is_trashed='NO'";


        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

    public function view(){

        $sqlQuery = "Select * from employee where ID=".$this->E_id;


        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $oneData =  $sth->fetch();

        return $oneData;
    }

    public function update(){

        $sqlQuery  = "Update employee set Name=?,Shift=? ,Salary = ? Where ID=".$this->E_id;


        $dataArray = [$this->E_Name, $this->E_shift, $this->E_salary];

        $sth = $this->dbh->prepare($sqlQuery);

        $status =  $sth->execute($dataArray);

        if($status)
            Message::setMessage("Success! Data has been updated successfully. <br>");
        else
            Message::setMessage("Failed! Data has not been updated. <br>");

    }

    public function delete(){

        $sqlQuery = "DELETE FROM employee WHERE ID=".$this->E_id;

        $status = $this->dbh->exec($sqlQuery);

        if($status)
            Message::setMessage("Success! Data has been deleted successfully. <br>");
        else
            Message::setMessage("Failed! Data has not been deleted. <br>");

    }

    public function trash(){

        $sqlQuery = "UPDATE employee SET is_trashed=NOW() WHERE ID=".$this->E_id;

        $status = $this->dbh->exec($sqlQuery);

        if($status)
            Message::setMessage("Success! Data has been trashed successfully. <br>");
        else
            Message::setMessage("Failed! Data has not been trashed. <br>");

    }

    public function trashed(){

        $sqlQuery = "Select * from employee WHERE is_trashed <> 'NO'";


        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

    public function recover(){


        $sqlQuery = "UPDATE employee SET is_trashed='NO' WHERE ID=".$this->E_id;

        $status = $this->dbh->exec($sqlQuery);

        if($status)
            Message::setMessage("Success! Data has been recovered successfully. <br>");
        else
            Message::setMessage("Failed! Data has not been recovered. <br>");

    }

    public function trashMultiple($selectedIDs){

        if( count($selectedIDs) == 0) {
            Message::message("Empty Selection! Please Select Some Record(s).");
            return;
        }

        $status = true;
        foreach ($selectedIDs as $id){

            $sqlQuery = "UPDATE employee SET is_trashed=NOW() WHERE ID=$id"  ;

            if ( ! $this->dbh-> exec($sqlQuery) )
                $status = false;
        }


        if($status)
            Message::message("Success! All Seleted Data Has Been Trashed");
        else
            Message::message("Failed! All Seleted Data Has Not Been Trashed");

    }

    public function recoverMultiple($selectedIDs){

        if( count($selectedIDs) == 0) {
            Message::message("Empty Selection! Please Select Some Record(s).");
            return;
        }

        $status = true;
        foreach ($selectedIDs as $id){

            $sqlQuery = "UPDATE employee SET is_trashed='NO' WHERE ID=$id"  ;

            if ( ! $this->dbh-> exec($sqlQuery) )
                $status = false;
        }


        if($status)
            Message::message("Success! All Seleted Data Has Been Recovered");
        else
            Message::message("Failed! All Seleted Data Has Not Been Recovered");

    }

    public function deleteMultiple($selectedIDs){

        if( count($selectedIDs) == 0) {
            Message::message("Empty Selection! Please Select Some Record(s).");
            return;
        }

        $status = true;
        foreach ($selectedIDs as $id){

            $sqlQuery = "DELETE FROM employee WHERE id=$id";

            if ( ! $this->dbh-> exec($sqlQuery) )
                $status = false;
        }


        if($status)
            Message::message("Success! All Seleted Data Has Been Deleted");
        else
            Message::message("Failed! All Seleted Data Has Not Been Deleted");

    }


    }