<?php
require_once ("../../vendor/autoload.php");

if(!isset($_SESSION)) session_start();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title> Admin login </title>

    <link rel="stylesheet" href="../../Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="../../Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <style>

        body {

            background-image: url("../../Resources/images/b1.jpg");
            background-repeat: no-repeat;
            background-size: cover;
        }

    </style>

</head>

<body>

<h3 align="center" style="color: #a94442"><?php echo @$_GET["invalid"]; ?></h3>

<div class="col-lg-5" style=" color: #1b6d85; background:rgba(0,0,0,0.2);margin-bottom: 150px; margin-left: 350px; border-radius: 10px;padding-top: 10px;padding-bottom: 10px;font-family: 'Comic Sans MS';margin-top: 50px">

    <h3 align="center" style="color: gainsboro"> Admin LogIn</h3>

    <form action="login.php" method="post">

    <div class="form-group">
        <label style="color:gainsboro;">Username</label>
        <input type="text" id="txtname" name="txtname">
    </div>


    <div class="form-group">
        <label style="color:gainsboro;">Password</label>
        <input type="password" id="txtpass" name="txtpass">
    </div>


    <button type="submit" class="btn btn-primary" style="margin-left: 100px">Login</button>

</form>
</div>

</body>
</html>
