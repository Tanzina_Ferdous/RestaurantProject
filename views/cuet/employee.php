<?php

require_once ("../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Message\Message;
use App\Restaurant\Restaurant;
use App\Utility\Utility;


$obj = new Restaurant();
$allData  =  $obj->index();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>document</title>

    <link rel="stylesheet" href="../style.css">

    <link rel="stylesheet" href="../../Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <script src="../../Resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

    <script src="../../Resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <style>

        body {

            background-image: url("../../Resources/images/b2.jpg");
            background-repeat: no-repeat;
            background-size: cover;
        }

    </style>

</head>

<body>

<div style="height: 20px">
    <div id="message" class="btn-success text-center" > <?php   echo Message::message();?>
    </div>
</div>

<div class="container">

    <form id="multiple"  method="post">


        <div class="nav navbar">
            <a href='create.php' class='btn btn-lg bg-success'>Create</a>
            <a href='trashed.php' class='btn btn-lg bg-danger'>On Leave Employees</a>


            <button id="TrashSelected" class='btn btn-lg bg-danger' style="color: royalblue">Send Selected To Leave</button>

            <button id="DeleteSelected" class='btn btn-lg bg-danger' style="color: royalblue">Delete Selected</button>


        </div>

    <div style="color: darkblue; font-size: 200%; "><h1> Active Employee List</h1></div>

    <table border="1px" class="table table-bordered table-striped">

        <tr style="font-size: larger">
            <th>Select all  <input id='select_all' type='checkbox' value='select all'></th>
            <th> Serial </th>
            <th> ID </th>
            <th> Employee Name </th>
            <th> Shift </th>
            <th> Salary </th>
            <th> Action Buttons </th>

        </tr>

        <?php

        $serial=1;

        foreach ($allData as $oneData){

            if($serial%2) $bgColor = "lightgoldenrodyellow";
            else $bgColor = "#ffffff";

            echo "
                        <tr  style='background-color: $bgColor ; background: rgba(200,200,200,0.2); font-size: larger'>
    
                        <td style='padding-left: 2%'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->ID'></td>
    
                        <td style='width: 10%; text-align: center'>$serial</td>
                        <td style='width: 10%; text-align: center'>$oneData->ID</td>
                        <td style='width: 20%;'>$oneData->Name</td>
                        <td style='width: 10%;'>$oneData->Shift</td>
                        <td style='width: 10%;'>$oneData->Salary</td>
    
                        <td>
                          <a href='view.php?id=$oneData->ID' class='btn btn-primary'>Detail</a>
                          <a href='edit.php?id=$oneData->ID' class='btn btn-success'>Edit</a>
                          <a href='trash.php?id=$oneData->ID' class='btn btn-warning'>ON Leave</a>
                          <a href='delete.php?id=$oneData->ID' onclick='return doConfirm()' class='btn btn-danger'>Delete</a>
                          </td>
                     </tr>
                                  
                   ";
            $serial++;
        }

        ?>

    </table>
</form>
</div>

<script>

    $(document).ready(function () {


        $("#TrashSelected").click(function () {
            $("#multiple").attr('action','trash_selected.php');
            $("#multiple").submit();
        });


        $("#DeleteSelected").click(function () {
            $("#multiple").attr('action','delete_selected.php');
            $("#multiple").submit();
        });

        $("#RecoverSelected").click(function () {
            $("#multiple").attr('action', 'recover_selected.php');
            $("#multiple").submit();
        });


    });

    function doConfirm() {

        var result = confirm("Are you sure you want to delete?");

        return result;


    }

    $(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    });

    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });

    </script>

</body>
</html>