<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit15d71233a747a5d87cec796d1f38eea3
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/cuet',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit15d71233a747a5d87cec796d1f38eea3::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit15d71233a747a5d87cec796d1f38eea3::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
