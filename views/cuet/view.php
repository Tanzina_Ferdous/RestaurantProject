<?php

require_once ("../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\Restaurant\Restaurant;


$obj = new Restaurant();
$obj->setData($_GET);

$oneData  =  $obj->view();

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>


    <link rel="stylesheet" href="../../Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <style>

        body {

            background-image: url("../../Resources/images/b8.jpg");
            background-repeat: no-repeat;
            background-size: cover;
        }
        </style>
</head>
<body>

<div class="container" style="width: 700px; margin-top: 100px ">


<?php

         echo "
             <h1> Single Employee Information </h1><br><br>
               
             <table class='table table-bordered' style='color: black; font-size: medium'>
             
                    <tr style='background-color: lightcyan'>                   
                        <td>  <b>Employee ID</b>  </td>                
                        <td>  <b>$oneData->ID</b>  </td>                
                      
                    </tr>
        
                     <tr style='background-color: lightcyan'>                   
                        <td>  <b>Employee Name</b>  </td>                
                        <td>  <b>$oneData->Name</b>  </td>                
                      
                    </tr>
                         
                     <tr style='background-color: lightcyan'>                   
                        <td>  <b>Shift</b>  </td>                
                        <td>  <b>$oneData->Shift</b>  </td>                
                      
                    </tr>
                     
                     <tr style='background-color: lightcyan'>                   
                        <td>  <b>Salary</b>  </td>                
                        <td>  <b>$oneData->Salary</b>  </td>                
                      
                    </tr>
             
             </table>

         ";


?>

</div>

</body>
</html>