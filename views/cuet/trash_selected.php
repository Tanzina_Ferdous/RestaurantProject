<?php

require_once ("../../vendor/autoload.php");

use App\Utility\Utility;
use App\Restaurant\Restaurant;
use App\Message\Message;

$obj = new Restaurant();

$selectedIDs =  $_POST['mark'];

$obj->trashMultiple($selectedIDs);


Utility::redirect("employee.php");
