<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

use App\Utility\Utility;

use App\Restaurant\Food;


$obj = new Food();
$obj->setData($_GET);

$obj->trash();

Utility::redirect("foodmenu.php");

?>
