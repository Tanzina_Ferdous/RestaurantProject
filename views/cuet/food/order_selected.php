<?php

require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Restaurant\Food;
use App\Message\Message;

$obj = new Food();

$selectedIDs =  $_POST['mark'];

$obj->trashMultiple($selectedIDs);


Utility::redirect("foodmenu.php");
