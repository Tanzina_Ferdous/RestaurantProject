<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Message\Message;
use App\Restaurant\Food;


$obj = new Food();
$allData  =  $obj->paid();


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>


    <link rel="stylesheet" href="../../../Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <script src="../../../Resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

    <style>

        body {

            background-image: url("../../../Resources/images/b3.jpg");
            background-repeat: no-repeat;
            background-size: cover;
        }

    </style>

</head>

<body>




<div class="container">

<form id="multiple" method="post">


    <div class="bg-info text-center"></div>
    <h1> Paid Items List</h1>

    <table border="1px" class="table table-bordered table-striped">

        <tr style="font-size: larger">

            <th> Serial </th>
            <th> Item NO </th>
            <th> Item Name </th>
            <th> Quantity </th>
            <th> Price </th>
        </tr>



        <?php


         $serial=1;

         foreach ($allData as $oneData){

             if($serial%2) $bgColor ="lightgoldenrodyellow";
             else $bgColor = "#ffffff";

             echo "
    
                        <tr  style='background-color: $bgColor; background: rgba(200,200,200,0.2); font-size: larger'>
    
                       
                        <td style='width: 5%; text-align: center'>$serial</td>
                        <td style='width: 10%; text-align: center'>$oneData->ID</td>
                        <td style='width: 20%;'>$oneData->NAME</td>
                        <td style='width: 15%;'>$oneData->QUANTITY</td>
                        
                        <td style='width: 10%;'>$oneData->PRICE</td>
                                     
    
                                     
                                  </tr>
                              ";
             $serial++;

         }

       ?>

    </table>

</form>


</div>



</body>
</html>