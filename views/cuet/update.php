<?php

if(!isset($_SESSION)) session_start();

require_once ("../../vendor/autoload.php");


use App\Utility\Utility;
use App\Message\Message;
use App\Restaurant\Restaurant;

$obj = new Restaurant();

$obj->setData($_POST);

$obj->update();

Utility::redirect('employee.php');




